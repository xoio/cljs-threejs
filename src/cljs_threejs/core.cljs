(ns cljs-threejs.core
  (:require ))

(enable-console-print!)


(defonce core
 (do
   (def renderer (js/THREE.WebGLRenderer.))
   (def scene (js/THREE.Scene.))
   (def camera (js/THREE.PerspectiveCamera. 75 (/ js/window.innerWidth js/window.innerHeight) 1.0 100000))

   (.setSize renderer js/window.innerWidth js/window.innerHeight)
   (.appendChild js/document.body (.-domElement renderer))
   (set! (.-position.z camera) 200)



   (.addEventListener
     js/window
     "resize"
     (fn []
         ))

   ))


(defn on-js-reload []
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)
